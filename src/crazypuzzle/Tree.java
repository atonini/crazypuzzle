package crazypuzzle;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
/**
 * @author Aline Tonini, Simone Rutz
 */
public class Tree {
    
    private int length;
    private int depth;
    private Node source;
    int visitedStates;
    // metodo construtor da arvores
    public Tree() {
        this.length = 0;
        this.depth = 0;
    }
    // 
    public int getLength() {
        return length;
    }

    public int getDepth() {
        return depth;
    }
    // retorna o nodo raiz
    public Node getSource() {
        return source;
    }
    //define o nodo raiz
    public void setSource(int[][] matrix){
        source = new Node();
        source.setMatrix(matrix);
        length++;
    }
    /**
     * 
     * @param finalMatrix
     */
    public Node startPuzzleWidth (){
        
        visitedStates = 0;
        ArrayList<int[][]> inserted = new ArrayList<> ();
        Node aux = source;
        ArrayList<Node> children;
        Queue<Node> myQueue = new LinkedList<>(); 
     
        aux.setVisited(true);
        inserted.add(source.getMatrix());
        
        
        while(!aux.isFinal()){
              
                createChildStates(aux);
                children = aux.getChildNode();
                length =+ aux.getChildNode().size();
                
                
                for(Node a : children){
                    if(!inserted.contains(a.getMatrix())){ // verify if the node alread exist
                        myQueue.add(a);
                        inserted.add(a.getMatrix());
                    }
                }
       
                if(myQueue.isEmpty()){
                    System.out.println("Não existe Solução!");
                    return null;
                }
                
                aux = myQueue.poll();
                
                visitedStates++;
        }
        System.out.println("Estados Visitados Amp" +visitedStates);
         System.out.println(aux.matrix[0][0] + " " + aux.matrix[0][1] + " " +aux.matrix[0][2]);
            System.out.println(aux.matrix[1][0] + " " + aux.matrix[1][1] + " " +aux.matrix[1][2]);
            System.out.println(aux.matrix[2][0] + " " + aux.matrix[2][1] + " " +aux.matrix[2][2]);

           System.out.println("  .  ");

           System.out.println("  .  ");
            System.out.println("  .  ");


        return aux;
    }
    //busca em profundidade
    public Node startPuzzleDepth (){
       
        visitedStates=0;
        Node aux = source;
        ArrayList<Node> children;
        Deque<Node> myDeque= new LinkedList<>(); 
        ArrayList<int[][]> inserted = new ArrayList<> ();
              
        aux.setVisited(true);
        inserted.add(source.getMatrix());
        
        
        while(!aux.isFinal()){
              
            createChildStates(aux);
            children = aux.getChildNode();
            length =+ aux.getChildNode().size();
              
            if(aux.getDepth()<5){  
                for(Node a : children){
                    if(!inserted.contains(a.getMatrix())){ // verify if this state alread exist
                        myDeque.push(a);
                        inserted.add(a.getMatrix());
                    }
                }
            }
                
            if(myDeque.isEmpty()){
                System.out.println("Não existe Solução!");
                return null;
            }
            
            
            System.out.println("Profundidade: " + aux.getDepth());
            aux = myDeque.pop();   
            visitedStates++;             
        }
        
        System.out.println("Estados Visitados Profu" +visitedStates);
         System.out.println(aux.matrix[0][0] + " " + aux.matrix[0][1] + " " +aux.matrix[0][2]);
            System.out.println(aux.matrix[1][0] + " " + aux.matrix[1][1] + " " +aux.matrix[1][2]);
            System.out.println(aux.matrix[2][0] + " " + aux.matrix[2][1] + " " +aux.matrix[2][2]);

           System.out.println("  .  ");

           System.out.println("  .  ");
            System.out.println("  .  ");
       
        return aux;
    }
    // retorna o caminho para a solução
    public LinkedList<Node> getTragetory(Node node){
     // a partir da solução retorna ate o nodo raiz   
        LinkedList<Node> tragetory = new LinkedList<>();
        Node aux;
        
        aux=node;
        
        while(!aux.isSource()){
            tragetory.add(aux);
            aux=aux.getParent();
        }
        
        tragetory.add(aux);
        
       return tragetory;
       
    }
    // cria estado
    private void createChildStates(Node node){
        
        int[][] matrix = node.getMatrix();
        int emptyBox = getEmptyBox(matrix);
        
        switch (emptyBox) {
                case 0: 
                                   
                    int[][] newMatrix1 = new int[][] {{matrix[1][0],matrix[0][1],matrix[0][2]},
                                                     {0,matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                     int[][] newMatrix2 = new int[][] {{matrix[0][1],0,matrix[0][2]},
                                                     {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    
                    break;
                    
                case 1:
                    int[][] newMatrix3 = new int[][] {{0,matrix[0][0],matrix[0][2]},
                                                     {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                  
                    int[][] newMatrix4 = new int[][] {{matrix[1][0],matrix[1][1],matrix[0][2]},
                                                     {matrix[1][0],0,matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix1 = new int[][] {{matrix[0][0],matrix[0][2],0},
                                                     {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    node.addNewChild(newMatrix3);
                    node.addNewChild(newMatrix4);
                    node.addNewChild(newMatrix1);
                    
                    break;
                    
                case 2:
                    newMatrix1 = new int[][] {{matrix[0][0],0,matrix[0][1]},
                                                     {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                  
                    newMatrix2 = new int[][] {{matrix[1][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[1][0],matrix[1][1],0},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    
                    break;
                    
                case 3:
                    newMatrix1 = new int[][] {{0,matrix[0][0],matrix[0][2]},
                                                     {matrix[0][0],matrix[1][1],matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                  
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                     {matrix[1][1],0,matrix[1][2]},
                                                     {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix3 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                     {matrix[2][0],matrix[1][1],matrix[1][2]},
                                                     {0,matrix[2][1],matrix[2][2]}};
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    node.addNewChild(newMatrix3);
                    
                    break;
                    
                case 4: 
                    newMatrix1 = new int[][] {{matrix[0][0],matrix[1][1],matrix[0][2]},
                                                       {matrix[1][0],0,matrix[1][2]},
                                                       {matrix[2][0],matrix[2][1],matrix[2][2]}};
                  
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {0,matrix[1][1],matrix[1][2]},
                                                      {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix3 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][2],0},
                                                      {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix4 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[2][1],matrix[1][2]},
                                                      {matrix[2][0],0,matrix[2][2]}};
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    node.addNewChild(newMatrix3);
                    node.addNewChild(newMatrix4);
                    
                    break;
                    
                case 5:
                     newMatrix1 = new int[][] {{matrix[0][0],matrix[0][1],0},
                                                       {matrix[1][0],matrix[1][2],matrix[0][2]},
                                                       {matrix[2][0],matrix[2][1],matrix[2][2]}};
                 
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],0,matrix[1][1]},
                                                      {matrix[2][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix3 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],matrix[2][2]},
                                                      {matrix[2][0],matrix[2][1],0}};
                  
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    node.addNewChild(newMatrix3);
                   
                    break;
                    
                case 6:
                      
                    newMatrix1 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {0,matrix[1][1],matrix[1][2]},
                                                      {matrix[1][0],matrix[2][1],matrix[2][2]}};
                    
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                      {0,matrix[2][0],matrix[2][2]}};
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    
                    break;
                    
                case 7:
                    newMatrix1 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],0,matrix[1][2]},
                                                      {matrix[2][0],matrix[1][1],matrix[2][2]}};
                    
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                      {0,matrix[2][0],matrix[2][2]}};
                    
                    newMatrix3 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                      {matrix[2][0],matrix[2][2],0}};
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    node.addNewChild(newMatrix3);
                    
                    break;
                    
                case 8:
                    newMatrix1 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],0},
                                                      {matrix[2][0],matrix[2][1],matrix[1][2]}}; 
                    
                    newMatrix2 = new int[][] {{matrix[0][0],matrix[0][1],matrix[0][2]},
                                                      {matrix[1][0],matrix[1][1],matrix[1][2]},
                                                      {matrix[2][0],0,matrix[2][1]}};  
                    
                    node.addNewChild(newMatrix1);
                    node.addNewChild(newMatrix2);
                    
                    break;       
                   
        }
        
        
    }
    // verifica o local vazio
    private static int getEmptyBox(int[][] matrix){
        
        int i, j, aux=0;
        
        for(i=0; i< 3; i++){
            for( j=0 ; j<3 ; j++){
                if(matrix[i][j] == 0){
                    return aux;
                }else{
                    aux++;
                }
            }
        }
        return -1;
    }    
}