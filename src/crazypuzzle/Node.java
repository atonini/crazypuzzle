package crazypuzzle;
import java.util.ArrayList;
/**
 * @author Aline Tonini, Simone Rutz
 */
public class Node {
    
    private int depth;
    int[][] matrix;
    private boolean visited;
    private ArrayList<Node> childNode;
    private Node parent;
   
    public Node() {
        matrix = new int[3][3];
        visited = false;
        parent = null;
        childNode = new ArrayList<Node>();
        depth = 0;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public int getDepth() {
        return depth;
    }

    public ArrayList<Node> getChildNode() {
        return childNode;
    }
    
    public Node getParent() {
        return parent;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public void setParent(Node parentNode) {
        this.parent = parentNode;
    }

    public boolean isVisited() {
        return visited;
    }
    
    public boolean isFinal(){
        
        return (matrix[0][0]==0) && (matrix[0][1]==1) && (matrix[0][2]==2) && (matrix[1][0]==3)
                && (matrix[1][1]==4)  && (matrix[1][2]==5) && (matrix[2][0]==6) && (matrix[2][1]==7) && (matrix[2][2]==8);
    }   
    
    public boolean isSource(){
        return parent == null;
    }
    
    
    public boolean hasChild(){
        return !childNode.isEmpty();
    }
    
    public void addNewChild(int[][] matrix){
        Node node = new Node();
        node.setMatrix(matrix);
        node.setParent(this);
        node.depth=this.depth+1;
        childNode.add(node);    
    }
    
    
       
    
    
}
