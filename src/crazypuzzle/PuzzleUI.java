package crazypuzzle;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

/**
 * @author Aline Tonini, Simone Rutz
 */
public class PuzzleUI extends javax.swing.JFrame {
   int[][] puzzle = new int[][] {{1,2,0},   // estado inicial
                                {3,4,5},
                                {6,7,8}};
   int[][] matrix = new int[][] {{0,1,2},   // estado final
                                 {3,4,5},
                                 {6,7,8}}; 
   String num;  // aux utilizado para ler o novo tabuleiro  
   Tree t1;     // declara arvore de busca
   Node res;    // recebe o resultado da busca
   LinkedList<Node> solucao = new LinkedList<>();   // lista que ira conter o caminho(nodos) para a solução 
   
    // classe que inicializa a interface grafica      
    public PuzzleUI() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        PuzzleBoard = new javax.swing.JPanel();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();
        B5 = new javax.swing.JButton();
        B6 = new javax.swing.JButton();
        B7 = new javax.swing.JButton();
        B8 = new javax.swing.JButton();
        B9 = new javax.swing.JButton();
        playButton = new javax.swing.JButton();
        busProfButton = new javax.swing.JRadioButton();
        busAmpButton = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        setPuzzleButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        numEstados = new javax.swing.JTextField();
        closeButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        numEstadoTotal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("8-Puzzle");
        setResizable(false);

        PuzzleBoard.setBackground(new java.awt.Color(255, 204, 204));
        PuzzleBoard.setBorder(javax.swing.BorderFactory.createTitledBorder("Puzzle Board"));
        PuzzleBoard.setToolTipText("");

        B1.setBackground(new java.awt.Color(248, 248, 249));
        B1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B1.setText("1");
        B1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B2.setText("2");
        B2.setToolTipText("");
        B2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        B3.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B3.setText("0");
        B3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3ActionPerformed(evt);
            }
        });

        B4.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B4.setText("3");
        B4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4ActionPerformed(evt);
            }
        });

        B5.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B5.setText("4");
        B5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5ActionPerformed(evt);
            }
        });

        B6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B6.setText("5");
        B6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6ActionPerformed(evt);
            }
        });

        B7.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B7.setText("6");
        B7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B7ActionPerformed(evt);
            }
        });

        B8.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B8.setText("7");
        B8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B8ActionPerformed(evt);
            }
        });

        B9.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        B9.setText("8");
        B9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PuzzleBoardLayout = new javax.swing.GroupLayout(PuzzleBoard);
        PuzzleBoard.setLayout(PuzzleBoardLayout);
        PuzzleBoardLayout.setHorizontalGroup(
            PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PuzzleBoardLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PuzzleBoardLayout.createSequentialGroup()
                        .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(B4))
                        .addGap(29, 29, 29)
                        .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(B5)
                            .addComponent(B2)
                            .addComponent(B8)))
                    .addComponent(B7))
                .addGap(30, 30, 30)
                .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(B3)
                    .addComponent(B6)
                    .addComponent(B9))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        PuzzleBoardLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {B1, B2, B3, B4, B5, B6, B7, B8, B9});

        PuzzleBoardLayout.setVerticalGroup(
            PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PuzzleBoardLayout.createSequentialGroup()
                .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B2)
                    .addComponent(B3))
                .addGap(26, 26, 26)
                .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B4)
                    .addComponent(B5)
                    .addComponent(B6))
                .addGap(35, 35, 35)
                .addGroup(PuzzleBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B7)
                    .addComponent(B8)
                    .addComponent(B9))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        PuzzleBoardLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {B1, B2, B3, B4, B5, B6, B7, B8, B9});

        playButton.setText("Play");
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

        busProfButton.setText("BuscaProfundidade");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, busProfButton, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), busProfButton, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        busProfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busProfButtonActionPerformed(evt);
            }
        });

        busAmpButton.setText("BuscaAmplitude");
        busAmpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busAmpButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Selecione o Algoritmo");

        setPuzzleButton.setText("New Puzzle");
        setPuzzleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setPuzzleButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Numero de Estado P/ Solucão:");

        numEstados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numEstadosActionPerformed(evt);
            }
        });

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        jButton1.setText("Mostra Caminho");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setText("Numero Total de Estados:");

        numEstadoTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numEstadoTotalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(busProfButton)
                    .addComponent(busAmpButton)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jButton1)
                    .addComponent(jLabel2)
                    .addComponent(playButton)
                    .addComponent(numEstados, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numEstadoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(setPuzzleButton)
                        .addGap(18, 18, 18)
                        .addComponent(closeButton)))
                .addGap(18, 18, 18)
                .addComponent(PuzzleBoard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(PuzzleBoard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(busProfButton)
                        .addGap(18, 18, 18)
                        .addComponent(busAmpButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(playButton)
                        .addGap(17, 17, 17)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(numEstadoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addGap(12, 12, 12)
                        .addComponent(numEstados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(setPuzzleButton)
                            .addComponent(closeButton))))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // classe que chama a busca selecionada
    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playButtonActionPerformed
        t1 = new Tree();    //cria arvore de busca
        t1.setSource(puzzle); // set como nodo raiz o estado inicial
        String aux;
        if(this.busProfButton.isSelected()){   // seleciona o metodo de busca
            //chamada da busca em profundidade
             JOptionPane.showMessageDialog(null,"chama busca em profundidade ", "Algoritmo selecionado", JOptionPane.PLAIN_MESSAGE);
             // mostrar o numero de estados visitados para encontrar a solução
             res = t1.startPuzzleDepth();
             aux = Integer.toString(t1.visitedStates);
             numEstadoTotal.setText(aux);
        } else{
            //chama da busca em amplitude
             JOptionPane.showMessageDialog(null,"chama busca em Amplitude ", "Algoritmo selecionado", JOptionPane.PLAIN_MESSAGE);
             // mostrar o numero de estados visitados para encontrar a solução
             res = t1.startPuzzleWidth();
             aux = Integer.toString(t1.visitedStates);
             numEstadoTotal.setText(aux);
        } 
    }//GEN-LAST:event_playButtonActionPerformed

   @SuppressWarnings("empty-statement")
   // reseta os campos do tabuleiro
    private void setPuzzleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setPuzzleButtonActionPerformed
        LinkedList<Integer> value = new LinkedList();
        Random random  = new Random();
        int tmp=0,flag=0;
         for(int i=0 ; i<3 ; i++){
            for(int j=0 ; j<3 ; j++){
                puzzle[i][j]=-1;
            }
        }
       
        while(value.size()!=9){
            tmp=random.nextInt(9);
            for (Integer value1 : value) {
                if (value1 == tmp) {
                    flag=1;
                }
            }
            if(flag==0) value.add(tmp);         
            flag=0;   
        }
        
        for(int i=0 ; i<3 ; i++){
            for(int j=0 ; j<3 ; j++){
                puzzle[i][j]=value.remove();
            }
        }
        String aux = Integer.toString(puzzle[0][0]);
        B1.setText(aux);
        aux = Integer.toString(puzzle[0][1]);
        B2.setText(aux);
        aux = Integer.toString(puzzle[0][2]);
        B3.setText(aux);
        aux = Integer.toString(puzzle[1][0]);
        B4.setText(aux);
        aux = Integer.toString(puzzle[1][1]);
        B5.setText(aux);
        aux = Integer.toString(puzzle[1][2]);
        B6.setText(aux);
        aux = Integer.toString(puzzle[2][0]);
        B7.setText(aux);
        aux = Integer.toString(puzzle[2][1]);
        B8.setText(aux);
        aux = Integer.toString(puzzle[2][2]);
        B9.setText(aux);
    }//GEN-LAST:event_setPuzzleButtonActionPerformed

    private void numEstadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numEstadosActionPerformed

    }//GEN-LAST:event_numEstadosActionPerformed

    private void B4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4ActionPerformed

    }//GEN-LAST:event_B4ActionPerformed

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed

    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed

    }//GEN-LAST:event_B2ActionPerformed

    private void B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3ActionPerformed

    }//GEN-LAST:event_B3ActionPerformed

    private void B5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5ActionPerformed

    }//GEN-LAST:event_B5ActionPerformed

    private void B6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6ActionPerformed

    }//GEN-LAST:event_B6ActionPerformed

    private void B7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B7ActionPerformed

    }//GEN-LAST:event_B7ActionPerformed

    private void B8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B8ActionPerformed

    }//GEN-LAST:event_B8ActionPerformed

    private void B9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B9ActionPerformed

    }//GEN-LAST:event_B9ActionPerformed
   
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
          System.exit(0);    // fecha o aplicativo
    }//GEN-LAST:event_closeButtonActionPerformed
   
    private void busProfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busProfButtonActionPerformed
          busAmpButton.setSelected(false);   // seleciona busca em profundidade
    }//GEN-LAST:event_busProfButtonActionPerformed
    
    private void busAmpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busAmpButtonActionPerformed
        busProfButton.setSelected(false);   // seleciona busca em amplitude
    }//GEN-LAST:event_busAmpButtonActionPerformed
   
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         // mostra os estado(caminho) para a solucao atravez do tabuleiro auterando seus campos
        solucao=t1.getTragetory(res);
        String aux, aux1;
        aux1 = Integer.toString(solucao.size());
        numEstados.setText(aux1);
       while(!solucao.isEmpty()){ 
            aux = Integer.toString(solucao.getLast().matrix[0][0]);
            System.out.println(aux);
            B1.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[0][1]);
            System.out.println(aux);
            B2.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[0][2]);
            System.out.println(aux);
            B3.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[1][0]);
            B4.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[1][1]);
            B5.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[1][2]);
            B6.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[2][0]);
            B7.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[2][1]);
            B8.setText(aux);
            aux = Integer.toString(solucao.getLast().matrix[2][2]);
            B9.setText(aux);
            solucao.removeLast();
             try {  
                 TimeUnit.SECONDS.sleep(2);  
            } catch (InterruptedException ignored) {  
                }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void numEstadoTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numEstadoTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numEstadoTotalActionPerformed
   
   
    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PuzzleUI().setVisible(true);
            }
            
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JButton B9;
    private javax.swing.JPanel PuzzleBoard;
    private javax.swing.JRadioButton busAmpButton;
    private javax.swing.JRadioButton busProfButton;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField numEstadoTotal;
    private javax.swing.JTextField numEstados;
    private javax.swing.JButton playButton;
    private javax.swing.JButton setPuzzleButton;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
