#####################################################################################################################
#                   crazyPuzzle  - Trabalho I de Inteligência Artificial                        		    #
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação 
#   3. Execução
#   
#   @autor Aline Rodrigues Tonini
#   @autor Simone Drawanz Rutz
#   @version 1.0
# 
#####################################################################################################################
#   1. Introduçao												    #
#####################################################################################################################
#
#   O projeto crazyPuzzle se propõem a realizar o trabalho I da disciplina de Inteligência Artificial, proposto pelo 
#   Prof. Anderson Ferrugem. O trabalho proposto é a implementação do 8-Puzzle buscando a solução através de busca em
#   largura e profundidade, verificando anteriormente se o puzzle possui solução.
#   
#####################################################################################################################
#   2. Implementação												    #
#####################################################################################################################
#
#   O trabalho foi implementado em Java utilizando interface gráfica e os algoritmos de busca em largura e profundidade.
#   
#   Classes:
#       PuzzleUI :
#
#
#       Node: Classe que implementa os estado de cada jogada. Possui uma matriz que representa o estado atual e referencias
#           para os próximos estados, bem como a profundidade na árvore de estados.
#
#       Tree: Classe que representa a árvore de estados e implementa os métodos de busca e criação de estados.
#
#####################################################################################################################
# 	3. Execução												    #
#####################################################################################################################
#   
#	
#
##################################################################################################################### 